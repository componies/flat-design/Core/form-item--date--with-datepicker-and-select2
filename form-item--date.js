(($) => {
  Drupal.behaviors.enhanced_dateinput = {
    attach: function attach(context, settings) {
      // Extend the datepicker with an aftershow trigger!
      $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
      $.datepicker._updateDatepicker = function(inst) {
          $.datepicker._updateDatepicker_original(inst);
          var afterShow = this._get(inst, 'afterShow');
          if (afterShow)
              afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
      };

      // Also show the datepicker on focus.
      $.datepicker.setDefaults({
        showOn: "focus"
      });

      /*
       * Disable the functionality where the datepicker always wants to be displayed,
       * inside the viewport (sometimes above and sometimes underneath the input-field)
       */
      $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset;}});

      var $wrapper = '<div class="select-style"></div>';
      var $icon = '<div class="icon"><svg viewBox="0 0 12 9" xmlns="http://www.w3.org/2000/svg"><path d="M6 9L0 0h12z" fill="#FFF" fill-rule="evenodd"/></svg></div>';
      var customDateFormat = 'yy-mm-dd';

      $('.form-item--date input[data-drupal-date-format]', context).once('custom_date_picker').each(function(){
        var $input = $(this);
        customDateFormat = $input.data('drupal-date-format');
        customDateFormat = customDateFormat.replace('Y', 'yy').replace('m', 'mm').replace('d', 'dd');

        $input.datepicker({
          dateFormat: customDateFormat,
          changeMonth: true,
          changeYear: true,
          yearRange: "c-90:c+10",
          afterShow: datepickerAfterShow
        });
      });

      function datepickerAfterShow(input, inst) {
        // Mimic the same semantics as Drupal selects. See component: form-item--select.
        $('select').once('enhanced_select')
          .wrap($wrapper)
          .parent()
          .append($icon)
          .find('select')
          .select2({
            minimumResultsForSearch: 2000
          });
      }
    }
  };
})(jQuery, Drupal);
